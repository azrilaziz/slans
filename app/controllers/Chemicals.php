<?php
class Chemicals extends Controller
{

    public function __construct()
    {
        //check if user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        ini_set('display_errors', 1);

        $this->modelChemical = $this->model('Chemical');
    }

    public function index()
    {
        //echo "user category id = " . $_SESSION['usercategoryid'] . "<br>";
        //exit();
        $chemical = $this->modelChemical->list();

        $data = [
            'title' => 'List Of Chemicals',
            'chemical' => $chemical
        ];

        $this->view('/chemicals/index', $data);
    }

    public function show()
    {
    }

    public function add()
    {

        if ($_SESSION['usercategoryid'] == '4') {
            redirect('pages/index');
        } else {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                //sanitize post data
                $_POST = filter_input_array(INPUT_POST, FILTER_UNSAFE_RAW);


                //if upload image
                if (file_exists($_FILES["chemicalimg"]["tmp_name"])) {

                    //get image dimension
                    $fileinfo = @getimagesize($_FILES["chemicalimg"]["tmp_name"]);

                    //print_r($_FILES["chemicalimg"]);
                    //exit();

                    $allowed_image_extension = [
                        "png", "jpg", "jpeg"
                    ];

                    //get image file extension

                    $file_extension = pathinfo($_FILES["chemicalimg"]["name"], PATHINFO_EXTENSION);
                    $file = pathinfo($_FILES["chemicalimg"]["name"]);
                    $imagename = time() . "_" . substr(str_replace(" ", "_", $file["filename"]), 0, 10) . "." . $file['extension'];

                    //validate file extension
                    if (!in_array($file_extension, $allowed_image_extension)) {
                        $data = [
                            "chemicalimg_err" => "Please upload valid image. Only png, jpg and jpeg are allowed"
                        ];

                        $this->view('chemicals/add', $data);
                    } else if (($_FILES["chemicalimg"]["size"]) > "2000000") {
                        $data = [
                            "chemicalimg_err" => "Image exceeds size of 2MB"
                        ];

                        $this->view('chemicals/add', $data);
                    } else {
                        //add image to your img root folder
                        $target = IMGROOT . "\chemicals\\" . $imagename;

                        if (move_uploaded_file($_FILES["chemicalimg"]["tmp_name"], $target)) {
                            $data = [
                                "chemicalimg_err" => ""
                            ];
                        } else {
                            $data = [
                                "chemicalimg_err" => "Problem in uploading images"
                            ];
                        }
                    }
                }



                $data = [
                    'title' => 'Add New Chemical',
                    'chemicalname' => trim($_POST['chemicalname']),
                    'chemicaldesc' => trim($_POST['chemicaldesc']),
                    'chemicalimg' => trim($imagename),
                    'chemicalname_err' => ''
                ];

                //validate data
                if (empty($data['chemicalname'])) {
                    $data['chemicalname_err'] = 'Please insert chemical name';
                }

                //make sure no errors
                if (empty($data['chemicalname_err'])) {
                    //insert data to database
                    if ($this->modelChemical->add($data)) {
                        flash('chemical_message', 'New chemical added');
                        redirect('chemicals/index');
                    } else {
                        die('something went wrong');
                    }
                } else {
                    //load view with errors
                    $this->view('chemicals/add', $data);
                }
            } else {
                $data = [
                    'title' => 'Add New Chemical',
                    'chemicalname' => '',
                    'chemicaldesc' => '',
                    'chemicalimg' => ''
                ];
            }
        }



        $this->view('chemicals/add', $data);
    }

    public function update($id)
    {
        if ($_SESSION['usercategoryid'] == '4') {
            redirect('pages/index');
        } else {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                //sanitize post data
                $_POST = filter_input_array(INPUT_POST, FILTER_UNSAFE_RAW);


                //if upload image
                if (file_exists($_FILES["chemicalimg"]["tmp_name"])) {

                    //get image dimension
                    $fileinfo = @getimagesize($_FILES["chemicalimg"]["tmp_name"]);

                    //print_r($_FILES["chemicalimg"]);
                    //exit();

                    $allowed_image_extension = [
                        "png", "jpg", "jpeg"
                    ];

                    //get image file extension

                    $file_extension = pathinfo($_FILES["chemicalimg"]["name"], PATHINFO_EXTENSION);
                    $file = pathinfo($_FILES["chemicalimg"]["name"]);
                    $imagename = time() . "_" . substr(str_replace(" ", "_", $file["filename"]), 0, 10) . "." . $file['extension'];

                    //validate file extension
                    if (!in_array($file_extension, $allowed_image_extension)) {
                        $data = [
                            "chemicalimg_err" => "Please upload valid image. Only png, jpg and jpeg are allowed"
                        ];

                        $this->view('chemicals/update', $data);
                    } //Validate image file size 
                    else if (($_FILES["chemicalimg"]["size"]) > "2000000") {
                        $data = [
                            "chemicalimg_err" => "Image exceeds size of 2MB"
                        ];

                        $this->view('chemicals/update', $data);
                    } else {
                        //add image to your img root folder
                        $target = IMGROOT . "\chemicals\\" . $imagename;

                        if (move_uploaded_file($_FILES["chemicalimg"]["tmp_name"], $target)) {
                            $data = [
                                "chemicalimg_err" => ""
                            ];
                        } else {
                            $data = [
                                "chemicalimg_err" => "Problem in uploading images"
                            ];
                        }
                    }
                } else {

                    $chemical = $this->modelChemical->getChemicalById($id);
                    $imagename = $chemical->chemicalimg;
                }



                $data = [
                    'title' => 'Update Chemical',
                    'chemicalid' => $id,
                    'chemicalname' => trim($_POST['chemicalname']),
                    'chemicaldesc' => trim($_POST['chemicaldesc']),
                    'chemicalimg' => trim($imagename),
                    'chemicalname_err' => ''
                ];

                //validate data
                if (empty($data['chemicalname'])) {
                    $data['chemicalname_err'] = 'Please insert chemical name';
                }

                //make sure no errors
                if (empty($data['chemicalname_err'])) {
                    //insert data to database
                    if ($this->modelChemical->update($data)) {
                        flash('chemical_message', 'Chemical updated');
                        redirect('chemicals/index');
                    } else {
                        die('something went wrong');
                    }
                } else {
                    //load view with errors
                    $this->view('chemicals/update', $data);
                }
            } else {
                //get existing data from model
                $chemical = $this->modelChemical->getChemicalById($id);

                $data = [
                    'title' => 'Update Chemical',
                    'chemicalid' => $id,
                    'chemicalname' => $chemical->chemicalname,
                    'chemicaldesc' => $chemical->chemicaldesc,
                    'chemicalimg' => $chemical->chemicalimg
                ];
            }
        }

        $this->view('chemicals/update', $data);
    }

    public function delete($id)
    {
        if ($_SESSION['usercategoryid'] == '4') {
            redirect('pages/index');
        } else {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if ($this->modelChemical->delete($id)) {
                    flash('chemical_message', 'Chemical have been deleted.');
                    redirect('chemicals/index');
                } else {
                    die('something went wrong.');
                }
            } else {
                redirect('chemicals/index');
            }
        }
    }
}
