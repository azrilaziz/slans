<?php
class Pages extends Controller
{

	public function __construct()
	{
		//check if user is logged in
		if (!isLoggedIn()) {
			redirect('users/login');
		}

		ini_set('display_errors', 1);

		$this->modelPage = $this->model('Page');
	}

	public function index()
	{

		//check if user is logged in
		if (!isLoggedIn()) {
			redirect('users/login');
		}

		$notibar = $this->modelPage->notification();

		$data =
			[
				'title' => 'SLANS - SMART LABORATORY NOTIFICATION SYSTEM',
				'notibar' => $notibar
			];


		$this->view('pages/index', $data);
	}

	public function about()
	{
		$data = [
			'title' => 'About Us'
		];

		//echo "this about page";
		$this->view('pages/about', $data);
	}

	public function contact()
	{

		echo "This is a contact page";
	}

	public function notification()
	{
		$notification = $this->modelPage->notification();

		$data = [
			'title' => 'Notifications',
			'notification' => $notification
		];

		//echo "this about page";
		$this->view('pages/notification', $data);
	}
}
