<?php
class Equipment extends Controller
{
    public function __construct()
    {
        //calling models
    }

    public function index()
    {

        $data = [
            'title' => 'Equipment'
        ];

        $this->view('equipment/index', $data);
    }

    public function create()
    {
    }

    public function update()
    {
    }

    public function delete()
    {
    }
}
