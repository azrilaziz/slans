<?php

class Notification
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function list()
    {

        $this->db->query('SELECT * FROM notification WHERE notidate <= CURDATE()');

        $result = $this->db->resultSet();

        return $result;
    }
}
