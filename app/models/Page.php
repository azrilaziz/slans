<?php
class Page
{

    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function notification()
    {
        $this->db->query('SELECT * FROM notification WHERE notidate <= CURDATE() LIMIT 5');

        $result = $this->db->resultSet();

        return $result;
    }
}
