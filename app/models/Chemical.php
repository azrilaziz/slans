<?php
class Chemical
{

    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function list()
    {
        $this->db->query('SELECT * FROM chemical');

        $result = $this->db->resultSet();

        return $result;
    }

    public function add($data)
    {

        $date = date('Y-m-d H:i:s');

        $this->db->query('INSERT INTO chemical (chemicalname, chemicaldesc, chemicalimg, createddate, createdby, updateddate, updatedby) VALUES (:chemicalname, :chemicaldesc, :chemicalimg, :createddate, :createdby, :updateddate, :updatedby)');

        //bind values
        $this->db->bind(':chemicalname', $data['chemicalname']);
        $this->db->bind(':chemicaldesc', $data['chemicaldesc']);
        $this->db->bind(':chemicalimg', $data['chemicalimg']);
        $this->db->bind(':createddate', $date);
        $this->db->bind(':createdby', $_SESSION['userid']);
        $this->db->bind(':updateddate', $date);
        $this->db->bind(':updatedby', $_SESSION['userid']);

        //execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getChemicalById($id)
    {
        $this->db->query('SELECT * FROM chemical WHERE chemicalid = :chemicalid');

        $this->db->bind(':chemicalid', $id);
        $row = $this->db->single();

        return $row;
    }

    public function update($data)
    {
        //print_r($data);

        $date = date('Y-m-d H:i:s');

        $this->db->query('UPDATE chemical SET chemicalname = :chemicalname
                          , chemicaldesc = :chemicaldesc
                          , chemicalimg = :chemicalimg
                          , updateddate = :updateddate
                          , updatedby = :updatedby
                          WHERE chemicalid = :chemicalid');

        $this->db->bind(':chemicalid', $data['chemicalid']);
        $this->db->bind(':chemicalname', $data['chemicalname']);
        $this->db->bind(':chemicaldesc', $data['chemicaldesc']);
        $this->db->bind(':chemicalimg', $data['chemicalimg']);
        $this->db->bind(':updateddate', $date);
        $this->db->bind(':updatedby', $_SESSION['userid']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $this->db->query('DELETE FROM chemical WHERE chemicalid = :chemicalid');

        $this->db->bind(':chemicalid', $id);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
