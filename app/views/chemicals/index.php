<!--begin::Header-->
<?php require APPROOT . '/views/inc/header.php' ?>
<?php flash('chemical_message'); ?>
<!--end::Header-->


<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Chemicals</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <!-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a> -->
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <!-- <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Today</a>
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Month</a>
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Year</a> -->
                <!--end::Actions-->
                <!--begin::Daterange-->

                <span class="text-muted font-size-base font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title">Today</span>
                <span class="text-primary font-size-base font-weight-bolder" id="kt_dashboard_daterangepicker_date"><?php echo date("d M Y"); ?></span>

                <!--end::Daterange-->

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label"><?= $data['title']; ?></h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="<?= URLROOT; ?>/chemicals/add" class="btn btn-primary font-weight-bolder">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <circle fill="#000000" cx="9" cy="15" r="6" />
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>New Record</a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Search Form-->
                    <!--begin::Search Form-->
                    <div class="mb-7">
                        <div class="row align-items-center">
                            <div class="col-lg-9 col-xl-8">
                                <div class="row align-items-center">
                                    <div class="col-md-4 my-2 my-md-0">
                                        <div class="input-icon">
                                            <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                            <span>
                                                <i class="flaticon2-search-1 text-muted"></i>
                                            </span>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                    <!--end::Search Form-->
                    <!--end: Search Form-->
                    <!--begin: Datatable-->
                    <table class="datatable datatable-bordered datatable-head-custom" id="kt_datatable">
                        <thead>
                            <tr>
                                <th title="Field #1">Chemical ID</th>
                                <th title="Field #2">Chemical Name</th>
                                <th title="Field #3">Description</th>
                                <th title="Field #4">Image</th>
                                <th title="Field #5">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data['chemical'] as $chemicals) : ?>
                                <tr>
                                    <td><?= $chemicals->chemicalid; ?></td>
                                    <td><?= $chemicals->chemicalname; ?></td>
                                    <td><?= substr($chemicals->chemicaldesc, 0, 50); ?></td>
                                    <td><?= $chemicals->chemicalimg; ?></td>
                                    <?php if ($_SESSION['usercategoryid'] != '4') { ?>
                                        <td>

                                            <a href="<?= URLROOT; ?>/chemicals/update/<?= $chemicals->chemicalid; ?>" class="btn btn-sm btn-clean mr-2 float-left" title="Edit details"><i class="fa  fa-pencil-alt"></i></a>

                                            <form action="<?= URLROOT; ?>/chemicals/delete/<?= $chemicals->chemicalid; ?>" method="POST" class="float-left">
                                                <input type="hidden" value="Delete" class="btn btn-sm btn-clean">

                                                <button class="btn btn-sm btn-clean mr-2" type="submit" onclick="return confirm('Are you sure to delete the item?');"><i class="fa fa-trash"></i></button>
                                            </form>



                                        </td>
                                    <?php } else { ?>
                                        <td>
                                            <a href="javascript:;" class="btn btn-sm btn-clean disabled mr-2" title="Edit details"><i class="fa  fa-pencil-alt"></i></a>
                                            <a href="javascript:;" class="btn btn-sm btn-clean disabled" title="Delete"><i class="fa fa-trash"></i></a>
                                        </td>

                                    <?php } ?>
                                </tr>
                            <?php endforeach;  ?>


                        </tbody>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->


<!--begin::Footer-->
<?php require APPROOT . '/views/inc/footer.php' ?>
<script src="<?= URLROOT; ?>/assets/js/pages/crud/ktdatatable/base/html-table.js"></script>

<!--end::Footer-->