<!--begin::Header-->
<?php require APPROOT . '/views/inc/header.php' ?>
<!--end::Header-->


<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Chemicals</h5>
                <!--end::Page Title-->
                <!--begin::Actions-->
                <!-- <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">#XRS-45670</span>
                <a href="#" class="btn btn-light-warning font-weight-bolder btn-sm">Add New</a> -->
                <!--end::Actions-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <!-- <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Today</a>
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Month</a>
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Year</a> -->
                <!--end::Actions-->
                <!--begin::Daterange-->

                <span class="text-muted font-size-base font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title">Today</span>
                <span class="text-primary font-size-base font-weight-bolder" id="kt_dashboard_daterangepicker_date"><?php echo date("d M Y"); ?></span>

                <!--end::Daterange-->

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 mb-0 pb-0">
                    <div class="card-title">
                        <h3 class="card-label"><?= $data['title']; ?></h3>
                    </div>
                </div>
                <div class="card-body">
                    <hr>
                    <form class="form" action="<?= URLROOT; ?>/chemicals/add" method="POST" name="frmchemadd" id="frmchemadd" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="mb-15">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-right">Chemical Name&nbsp;<span style="color:red;">*</span>:</label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control <?php echo (!empty($data['chemicalname_err'])) ? 'is-invalid' : ''; ?>" name="chemicalname" id="chemicalname" placeholder="Enter chemical name" />
                                        <span class="form-text <?php echo (!empty($data['chemicalname_err'])) ? 'invalid-feedback' : 'text-muted'; ?>">Please enter chemical name</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-right">Description:</label>
                                    <div class="col-lg-6">
                                        <textarea name="chemicaldesc" id="chemicaldesc" cols="100" rows="10" class="form-control"></textarea>
                                        <span class="form-text text-muted">Chemical Description</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label text-right">Image:</label>
                                    <div class="image-input col-lg-2" id="kt_image_2">
                                        <div class="image-input-wrapper" style="background-image: url(<?= URLROOT; ?>/assets/media/chemicals/chemical-default.jpg)"></div>

                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change Chemical Image">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="chemicalimg" accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" name="profile_avatar_remove" />
                                        </label>

                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel Chemical Image">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                    <a href="<?= URLROOT; ?>/chemicals/index" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>
<!--end::Content-->


<!--begin::Footer-->
<?php require APPROOT . '/views/inc/footer.php' ?>
<script src="<?= URLROOT; ?>/assets/js/pages/crud/ktdatatable/base/html-table.js"></script>
<script src="<?= URLROOT; ?>/assets/js/pages/crud/file-upload/image-input.js"></script>
<!--end::Footer-->